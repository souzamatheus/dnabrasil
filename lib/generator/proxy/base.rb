# encoding: utf-8
module Generator
  module Proxy
    class Base 
      require "prawn"
       include ApplicationHelper
        

      def initialize(service_document)
        @document = Prawn::Document.new(margin: [80, 40, 80, 40])
        @service_document = service_document
        @document.font("Times-Roman")
        @document.default_leading = 5
      end

      def generate
        header
        document_title
        document_box
        document_content
        add_city_and_date
        add_client_signature  
        footer
        add_page_numbers
        @document.render
      end

      private

      def add_page_numbers
        options = {
          at: [@document.bounds.right - 140, -20],
          width: 150,
          align: :right,
          start_count_at: 1,
        }

        @document.number_pages("<page>/<total>", options)
      end

      def add_client_signature
        text = @service_document.person_name
        @document.text_box("_" * (text.length + 10), at: [0, 50], align: :right)
        @document.text_box("#{text}", at: [0, 22], align: :right)
      end

      def add_city_and_date
        @document.text_box("#{@service_document.city}, #{I18n.l(Time.now, format: :local)}", at: [0, 120], align: :right)
      end

      def footer
        @document.repeat(:all) do
          @document.bounding_box([0, @document.bounds.bottom - 35], width: 540, height: 100) do
            @document.stroke_horizontal_rule
            @document.move_down(5)
            @document.text("Rua Agostinho Gomes, nº3095, Ipiranga, São Paulo – SP - CEP 04206-002", align: :center)
            @document.text("Fone: (11) 5061.1161 www.dnabrasilmarcas.com.br", align: :center)
            
          end
        end
      end

      def document_box
        @document.text("<b>#{@service_document.company_name}</b> empresa brasileira, estabelecida á "+
          "#{@service_document.address} - CEP #{@service_document.postal_code} - #{@service_document.city} - #{@service_document.state} "+
          "devidamente inscrita no CNPJ #{formatted_cnpj(@service_document.client_document)}, neste ato legalmente representada por #{@service_document.person_name}, " + 
          " #{@service_document.nationality}, #{@service_document.relationship_status_humanize} e portador do CPF " +
          " #{formatted_cpf(@service_document.cpf)} ", align: :justify, inline_format: :true)
         



      end

      def document_content
        @document.move_down(15)
        @document.text("Pelo presente instrumento de procuração, nomeia e constitui sua bastante " +
          "Procuradora:<b> #{I18n.t('project.company_name')}</b>, " +
          "empresa brasileira, inscrita sob o CNPJ 14.798.186/0001-80, estabelecida na " +
          "Cidade de São Paulo- SP, à Rua Agostinho Gomes, nº 3095, Ipiranga, CEP " +
          "04206-002, neste ato representada por DANILA PAULA BAETA MARTINS, portadora do " +
          "CPF 351.965.158-01 e RG 34.245.937-5, brasileira, casada e empresária, " +
          "com poderes para ISOLADAMENTE, representar a Outorgante junto ao MINISTÉRIO " +
          "DA JUSTIÇA, especialmente frente ás JUNTAS COMERCIAIS ESTADUAIS, e ao " +
          "INSTITUTO NACIONAL DA PROPRIEDADE INDUSTRIAL (INPI), e requerer em nome da " +
          "Outorgante: proteção ao nome comercial, registros de marcas de indústria, " +
          "comércio ou serviços, expressão sinal de propaganda, patente de invenção, " +
          "modelo de utilidade, desenho industrial, garantia de prioridade, anotações, " +
          "cessão de marcas e patentes, caducidades e nulidade, bem como apresentar " +
          "oposições e replicar as por outrem oferecida, recorrer e triplicar, retirar " +
          "certidões e certificados, provar o uso efetivo, desistir, renunciar, " +
          "invocar processos, cumprir exigências e oferecer contestações, pagar taxas, " +
          "relativas aos assuntos da Propriedade Industrial, desde a data do depósito " +
          "e durante a vigência do privilégio de registro, requerendo e apresentando " +
          "em suma, tudo o que for necessário a bem dos direitos da outorgante, " +
          "ratificar e retificar ou  eventualmente já praticados, ou ainda substalecer "  +
          "esta em outrem, com ou sem reservas de iguais poderes, dando por tudo por " +
          "bom, firme e valioso.",align: :justify, inline_format: true)
      end

    

      def document_title
        @document.move_down(20)
        @document.text("<u> <b> PROCURAÇÃO </u>", size: 18, align: :center, inline_format: true)
        @document.move_down(20)
      end

      def header
        @document.repeat(:all) do
          @document.move_up(40)
          @document.image(File.join(Rails.root, "app/assets/images", "logo.png"), width: 120)
          @document.text("DNA BRASIL - GESTÃO DE MARCAS", align: :right, size: 12)
          @document.stroke_horizontal_rule
          @document.move_down(40)
        end
      end






    end
  end
end
