# encoding: utf-8
module Generator
  module Contract
    class Base
      require "prawn"
      include ApplicationHelper

      def initialize(service_document)
        @document         = Prawn::Document.new(margin: [80, 40, 80, 40])
        @service_document = service_document
        @document.font("Times-Roman")
        @document.default_leading = 5
        @clauses = Clauses.new(@document, service_document)
      end

      def generate
        header
        document_title
        add_hirer
        add_contractor
        @clauses.add_clauses_to_document
        add_term
        add_city_and_date
        add_client_signature
        add_company_signature
        add_witnesses
        footer
        add_page_numbers
        @document.render
      end

      private

      def add_page_numbers
        options = {
          at: [@document.bounds.right - 140, -20],
          width: 150,
          align: :right,
          start_count_at: 1,
        }

        @document.number_pages("<page>/<total>", options)
      end

      def add_witnesses
        @document.move_cursor_to(140)
        @document.text("<b>TESTEMUNHAS:</b>", inline_format: true)
        @document.move_down(10)
        @document.text("<b>1.</b>#{'_' * 50}", inline_format: true)
        @document.text("<b>2.</b>#{'_' * 50}", inline_format: true)
        @document.text("Nome:#{' ' * 45} Nome:")
        @document.text("Rg:#{' ' * 50} Rg:")
      end

      def add_client_signature
        text = @service_document.company_name
        @document.text_box("_" * (text.length + 10), at: [0, 280], align: :right)
        @document.text_box("#{text}", at: [0, 265], align: :right)
      end

      def add_company_signature
        text = I18n.t('project.company_name')
        @document.text_box("_" * (text.length + 10), at: [0, 220], align: :right)
        @document.text_box(text, at: [0, 205], align: :right)
      end

      def add_city_and_date
        @document.text_box("#{@service_document.city}, #{I18n.l(Time.now, format: :local)}", at: [0, 340], align: :right)
      end

      def add_term
        @document.move_down(20)
        @document.text(I18n.t("generator.contract.term"))
        @document.move_down(20)
      end

      def footer
        @document.repeat(:all) do
          @document.bounding_box([0, @document.bounds.bottom - 35], width: 540, height: 100) do
            @document.stroke_horizontal_rule
            @document.move_down(5)
            @document.text("Rua Agostinho Gomes, nº3095, Ipiranga, São Paulo – SP - CEP 04206-002", align: :center)
            @document.text("Fone: (11) 5061.1161 www.dnabrasilmarcas.com.br", align: :center)
          end
        end
      end

      def document_title
        @document.text("<b> CONTRATO DE PRESTAÇÃO DE SERVIÇOS </b>", align: :center, inline_format: true)
        @document.move_down(40)
      end

      def header
        @document.repeat(:all) do
          @document.move_up(60)
          @document.image(File.join(Rails.root, "app/assets/images", "logo.png"), width: 120)
          @document.text("DNA BRASIL - GESTÃO DE MARCAS", align: :right, size: 12)
          @document.stroke_horizontal_rule
          @document.move_down(40)
        end
      end

      def add_hirer
        @document.text("O presente instrumento particular é celebrado entre as partes, a saber:")
        @document.move_down(20)
        @document.bounding_box([25, @document.cursor], width: 500, height: 150) do
          @document.text(hirer_informations, inline_format: true)
        end
      end

      def add_contractor
        @document.bounding_box([25, @document.cursor], width: 500, height: 170) do
          @document.text("(b) <b>#{I18n.t('project.company_name')}</b>, com sede à Rua Agostinho Gomes, nº 3095, Ipiranga, CEP 04206-002, "+
                    "São Paulo - SP, devidamente inscrita no CNPJ sob o n°14.798.186/0001-80, neste ato legalmente representada por <b>DANILA PAULA BAETA MARTINS</b>, "+
                    "portadora do CPF do MF sob o nº 351.965.158-01 e RG sob o nº 34.245.937-5, brasileira, casada e empresária, com escritório profissional à Rua " +
                    "Agostinho Gomes, 3095, Ipiranga, CEP 04206-002, São Paulo - SP, doravante denominada simplesmente <b>CONTRATADA</b>, tem entre si " +
                    "justo e acordado o presente contrato de prestação de serviços, o qual será regido pelas cláusulas e condições a seguir, bem " +
<<<<<<< HEAD
                    "como pela legislação em vigor.", align: :justify, inline_format: true)
=======
<<<<<<< HEAD
                    "como pela legislação em vigor.", align: :justify, inline_format: true)
=======
                    "como pela legislação em vigor.", inline_format: true)
>>>>>>> origin/master
>>>>>>> origin/master
        end
      end

      def hirer_informations
        options = {
          type:         @service_document.client.document_type,
          company_name: @service_document.company_name,
          person_name:  @service_document.person_name,
          nationality:  @service_document.nationality.downcase,
          relationship: @service_document.relationship_status_humanize.downcase,
          occupation:   UnicodeUtils.downcase(@service_document.occupation),
          address:      "#{@service_document.full_address} - CEP #{@service_document.postal_code} - #{@service_document.city} - #{@service_document.state}",
          cpf:          formatted_cpf(@service_document.cpf),
          cnpj:         formatted_cnpj(@service_document.client_document),
          rg:           @service_document.rg
        }

        I18n.t("generator.contract.hirer_#{options[:type]}", options)
      end
    end
  end
end
