module Generator
  module Contract
    class Clauses
      attr_reader :document

      def initialize(document, service_document)
        @document         = document
        @service_document = service_document
      end

      def add_clauses_to_document
        (1..13).each do |clause_number|
          add_clause(clause_number)
        end
      end

      def get_text(position, type, options={})
        I18n.t("generator.contract.clause.#{position.humanize}.#{type}", {default: ""}.merge(options))
      end

      def get_paragraph(clause, position, type)
        I18n.t("generator.contract.clause.#{clause.humanize}.paragraph.#{position.humanize}.#{type}", default: "")
      end

      private

      def add_clause(number)
        options   = {inline_format: true}
        title     = get_text(number, "title")
        subtitle  = get_text(number, "subtitle")
        content   = get_text(number, "content", {
                      month:          @service_document.client.translate_annuity.downcase,
                      amount:         @service_document.amount,
                      payment_method: @service_document.contract.payment
                    })

        add_text(title, options)
        add_text("#{subtitle}: #{content}", options)
        add_paragraphs(number)
      end

      def add_paragraphs(clause)
        (1..3).each do |paragraph_number|
          paragraph_title   = get_paragraph(clause, paragraph_number, "title")
          paragraph_content = get_paragraph(clause, paragraph_number, "content")

          if paragraph_title.present? and paragraph_content.present?
            add_text("#{paragraph_title}: #{paragraph_content}", {inline_format: true})
          end
        end

        @document.move_down(10)
      end

      def add_text(text, options={})
        if text.present?
          @document.move_down(20)
          @document.text(text, options)
        end
      end
    end
  end
end
