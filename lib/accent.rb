# encoding: UTF-8
class Accent
  ACCENTS = {
    %w{ a ã á â } => %w{ a ã á â },
    %w{ e é ê }   => %w{ e é ê },
    %w{ i í }     => %w{ i í },
    %w{ o õ ó ô } => %w{ o õ ó ô },
    %w{ u ú }     => %w{ u ú },
    %w{ c ç }     => %w{ c ç }
  }

  def self.to_regexp(string)
    sanitized = string.clone
    ACCENTS.each_pair do |key, value|
      sanitized.gsub!(Regexp.union(key), "[#{value.join}]")
    end

    sanitized
  end
end
