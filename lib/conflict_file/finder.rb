module ConflictFile
  class Finder < Base
    def matcher
      log = Logger.new('log/colidencia.log')
      brands = extract_brands

      finded_clients = brands.inject([]) do |clients, info|
        log.info("Brand: #{info.brand}")

        search_possibilities(info.brand).each do |word|
          log.info("\tSearching clients for: #{word}")
          client = find_client(word)
          clients.push(add_informations(client, info, word)) if client.present?
        end

        clients
      end

      finded_clients
    end

    private

    def find_client(word)
      Client.find_conflict(word).first

      rescue Exception => e
        nil
    end

    def add_informations(client, info, word)
      OpenStruct.new(
        number:       info.number,
        date:         info.date,
        class_name:   info.class_name,
        company_name: info.company_name,
        inpi:         info.inpi,
        presentation: info.presentation,
        nature:       info.nature,
        brand:        info.brand,
        conflict:     word,
        client:       client
      )
    end

    def search_possibilities(brand)
      exclude_caracters(group_words(brand))
    end

    def exclude_caracters(words)
      regexp = /^[!@#$\%^&*\(\)-]$|^d[aeiou]s?+$|^[aeiou]+$|(\[|\])+/
      words.delete_if do |word|
        word.match(regexp)
      end
    end

    def group_words(brand)
      brand.gsub!(/[\(\)\|]/, " ")
      splitted  = brand.split(" ")
      group     = [brand, splitted].flatten
      splitted.each_with_index do |_, index|
        group.push("#{splitted[index]} #{splitted[index+1]}".strip)
      end

      group.uniq
    end
  end
end
