# encoding: utf-8
module TrackerDocument
  extend self

  DOCUMENTS = {
    "AwareDocument" => "Carta de Ciência",
    "Contract" => "Contrato",
    "Proxy" => "Procuração",
    "ServiceDocument" => "Autorização de serviço"
  }

  ACTION = {
    "create" => "gerado"
  }

  def register(client, user, document_name, action)
    action = ACTION.fetch(action.downcase)
    entry = {
      description: "Documento #{action}: #{DOCUMENTS[document_name]}",
      status: SacStatus::INTERNAL_PROCESS,
      user_id: user.id
    }

    SacManager.new(client, entry).save
  end
end
