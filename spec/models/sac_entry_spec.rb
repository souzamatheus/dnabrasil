require 'spec_helper'

describe SacEntry do
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:client_id) }

  it { should belong_to(:user) }
  it { should belong_to(:client) }

  describe '#description' do
    context 'less than ten caracters' do
      let(:entry) { build(:sac_entry, description: 'invalid') }
      it { expect(entry).to_not be_valid }
    end

    context 'more than ten caracters' do
      let(:entry) { build(:sac_entry) }
      it { expect(entry).to be_valid }
    end
  end
end
