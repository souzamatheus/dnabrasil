require 'spec_helper'

describe User do
  it { should validate_presence_of(:login) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:role) }
  it { should validate_presence_of(:encrypted_password) }

  it { should validate_presence_of(:password) }
  it { should validate_confirmation_of(:password) }
  it { should validate_length_of(:password).within(Devise.password_length) }

  it { should validate_uniqueness_of(:login) }
  it { should validate_inclusion_of(:role).to_allow("admin", "manager", "operator") }

  it { should have_many(:clients) }

  describe '#lockable?' do
    context 'admin' do
      let(:user){ create(:user, :admin) }
      it {user.should_not be_lockable }
    end

    context 'manager' do
      let(:user){ create(:user, :manager) }
      it {user.should_not be_lockable }
    end

    context 'consultant' do
      let(:user){ create(:user, :consultant) }
      it {user.should be_lockable }
    end

    context 'operator' do
      let(:user){ create(:user, :operator) }
      it {user.should be_lockable }
    end
  end

  describe '#translate_role' do
    context 'Admin' do
      let(:user){ create :user, :admin }

      it { user.translate_role.should == 'Administrador' }
    end

    context 'Manager' do
      let(:user){ create :user, :manager }

      it { user.translate_role.should == 'Supervisor' }
    end

    context 'Consultant' do
      let(:user){ create :user, :consultant }

      it { user.translate_role.should == 'Consultor' }
    end

    context 'Operator' do
      let(:user){ create :user, :operator }

      it { user.translate_role.should == 'Atendente' }
    end
  end

  describe '#initial_password' do
    context 'without password' do
      let(:user){ build :user, login: 'rocky', password: nil }
      before { user.save }

      it { user.valid_password?('rockydnabrasil').should be true }
    end

    context 'with password' do
      let(:user){ build :user, login: 'rocky', password: 'mypass' }
      before { user.save }

      it { user.valid_password?('mypass').should be true }
    end
  end

  describe '#reset_password' do
    let(:user){ create :user, login: 'rocky' }
    before { user.reset_password }

    it { user.valid_password?('rockydnabrasil').should be true }
  end

  describe 'lock and unlock a user' do
    let(:user){ create :user }
    let(:client){ create :client, company_name: 'Rocky' }

    context '#lock' do
      it { user.constraints.should == { blocked: false, client: nil } }

      it 'locking a user' do
        user.lock(client)
        user.constraints.should == { blocked: true, client: client.id }
      end
     end

    context '#unlock' do
      before { user.lock client }

      it 'unlocking a user' do
        user.unlock
        user.constraints.should == { blocked: false, client: nil }
      end
     end
   end
end
