require 'spec_helper'

describe Historic do
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:status) }

  it { should be_embedded_in(:sac) }
  it { should belong_to(:user) }
end