require 'spec_helper'

describe Notification do
  it { should validate_presence_of :call_back_at }
  it { should validate_presence_of :notification_type }

  it { should belong_to :user }
  it { should belong_to :client }
end
