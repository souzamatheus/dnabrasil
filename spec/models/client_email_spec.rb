require 'spec_helper'

describe ClientEmail do
  it {validate_uniqueness_of(:email) }
  it {validate_format_of(:email).to_allow("rocky@rockyland.com").not_to_allow("wrong email") }
end
