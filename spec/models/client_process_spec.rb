require 'spec_helper'

describe ClientProcess do
  it { should be_embedded_in(:client) }

  it { should allow_mass_assignment_of :number }
  it { should allow_mass_assignment_of :filing_at }
  it { should allow_mass_assignment_of :grant_at }
  it { should allow_mass_assignment_of :validity_at }

  it { should_not allow_mass_assignment_of :start_ordinary_appeal }
  it { should_not allow_mass_assignment_of :finish_ordinary_appeal }
  it { should_not allow_mass_assignment_of :start_extraordinary_appeal }
  it { should_not allow_mass_assignment_of :finish_extraordinary_appeal }

  describe 'process informations' do
    subject {Client.last.client_processes.last}

    before do
      client = create(:client)

      client.client_processes.create(
        filing_at: Date.new(2006, 10, 30),
        grant_at: Date.new(2010, 3, 2),
        validity_at: Date.new(2020, 3, 2),
      )
    end

    context 'new informations' do
      its(:filing_at) {should == Date.new(2006, 10, 30)}
      its(:grant_at) {should == Date.new(2010, 3, 2)}
      its(:validity_at) {should == Date.new(2020, 3, 2)}

      its(:start_ordinary_appeal) {should == Date.new(2019, 3, 3)}
      its(:finish_ordinary_appeal) {should == Date.new(2020, 3, 2)}
      its(:start_extraordinary_appeal) {should == Date.new(2020, 3, 3)}
      its(:finish_extraordinary_appeal) {should == Date.new(2020, 9, 2)}
    end

    context 'update informations' do
      let!(:process) do
        process = Client.last.client_processes.last
        process.update_attributes(validity_at: Date.new(2030, 10, 20))
        process
      end

      its(:validity_at) {should == Date.new(2030, 10, 20)}

      its(:start_ordinary_appeal) {should == Date.new(2029, 10, 20)}
      its(:finish_ordinary_appeal) {should == Date.new(2030, 10, 20)}
      its(:start_extraordinary_appeal) {should == Date.new(2030, 10, 21)}
      its(:finish_extraordinary_appeal) {should == Date.new(2031, 4, 20)}
    end
  end
end
