require 'spec_helper'

describe ServiceDocument do
  it { should validate_presence_of(:number) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:agency) }
  it { should validate_presence_of(:amount) }
  it { should validate_presence_of(:payment) }

  it { should validate_presence_of(:person_name) }
  it { should validate_presence_of(:nationality) }
  it { should validate_presence_of(:relationship_status) }
  it { should validate_presence_of(:rg) }
  it { should validate_presence_of(:cpf) }
  it { should validate_presence_of(:birth_date) }
  it { should validate_presence_of(:occupation) }
  it { should validate_presence_of(:position) }
  it { should validate_presence_of(:agent) }

  it { should validate_presence_of(:company_name) }
  it { should validate_presence_of(:address) }
  it { should validate_presence_of(:neighborhood) }
  it { should validate_presence_of(:city) }
  it { should validate_presence_of(:state) }
  it { should validate_presence_of(:postal_code) }
  it { should validate_presence_of(:client_document) }
  it { should validate_presence_of(:phone) }
  it { should validate_presence_of(:contact) }

  it { should embed_one(:contract) }

  describe "#full_address" do
    context 'without a complement' do
      let(:document){ create :service_document, :client_informations }
      it { document.full_address.should == "Rua tal, 134" }
    end

    context 'with a complement' do
      let(:document){ create :service_document, :client_informations, :complement }
      it { document.full_address.should == "Rua tal, 134, apto 103 bloco 01" }
    end
  end

  describe "#validate_cpf" do
    before { service_document.valid? }

    context 'when is nil' do
      let(:service_document){ ServiceDocument.new }
      it { service_document.should have(2).errors_on(:cpf) }
    end

    context 'when is invalid' do
      let(:service_document){ ServiceDocument.new(cpf: 'wrong') }
      it { service_document.should have(1).errors_on(:cpf) }
    end

    context 'when is valid' do
      let(:service_document){ ServiceDocument.new(cpf: '713.808.482-02') }
      it { service_document.should have(0).errors_on(:cpf) }
    end
  end
end
