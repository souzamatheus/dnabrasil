#encoding: utf-8
require 'spec_helper'

describe Client do
  it { should validate_presence_of(:company_name) }
  it { should_not validate_presence_of(:brand) }
  it { should validate_presence_of(:annuity) }
  it { should validate_presence_of(:primary_phone) }
  it { should validate_presence_of(:contact) }

  it { should validate_inclusion_of(:annuity).to_allow("may", "november") }

  it { should belong_to(:user) }
  it { should embed_one(:sac) }
  it { should embed_many(:client_processes) }
  it { should embed_many(:brands) }
  it { should embed_many(:conflicts) }
  it { should have_many(:service_documents) }
  it { should have_many(:sac_entries) }

  describe '.find_conflict' do
    describe 'find a client depending on his status' do
      let(:word) {'spfc'}
      before do
        conflict = build(:conflict, value: word)
        create(:client, conflicts: [conflict], status: states.sample)
      end

      context 'with an available status' do
        let(:states) {%w(active entry_active entry_cancelled without_monitoring)}
        it {Client.find_conflict(word).should have(1).item}
      end

      context 'without an available status' do
        let(:states) {%w(negotiating canceled refused)}
        it {Client.find_conflict(word).should be_empty}
      end
    end
  end

  describe '#translate_annuity' do
    context 'May' do
      let(:client){ create :client, annuity: 'may' }

      it { client.translate_annuity.should == 'Maio' }
    end

    context 'November' do
      let(:client){ create :client, annuity: 'november' }

      it { client.translate_annuity.should == 'Novembro' }
    end
  end

  describe 'clean document' do
    context 'when is people' do
      let(:client){ build(:client, cpf: '352.777.458-07') }

      it 'should clean . - from cpf' do
        client.save
        client.reload.cpf.should == '35277745807'
      end
    end

    context 'when is company' do
      let(:client){ build(:company, cnpj: '43.879.842/0001-78') }

      it 'should clean the cnpj' do
        client.save
        client.reload.cnpj.should == '43879842000178'
      end
    end
  end

  describe "cpf/cnpj validation" do
    context 'cpf' do
      let(:client){ build :client, cpf: '000.000.000-00' }
      it { client.should be_invalid }

      context 'errors' do
        before { client.valid? }

        it { client.should have(1).error_on(:cpf) }
      end
    end

    context 'cnpj' do
      let(:company){ build :company, cnpj: '00.000.000/0001-00' }
      it { company.should be_invalid }

      context 'errors' do
        before { company.valid? }

        it { company.should have(1).error_on(:cnpj) }
      end
    end

    context 'negotiating' do
      let(:client){ build :client, status: "negotiating", cpf: nil }
      let(:company){ build :company, status: "negotiating", cnpj: nil }

      it { company.should be_valid }
      it { client.should be_valid }
    end
  end

  describe '#validations' do
    context 'status active without annuity' do
      let(:client){ build :client, annuity: nil, status: 'active' }
      it { client.should_not be_valid }
    end

    context 'status active entry without annuity' do
      let(:client){ build :client, annuity: nil, status: 'entry_active' }
      it { client.should_not be_valid }
    end

    context 'status without_monitoring without annuity' do
      let(:client){ build :client, annuity: nil, status: 'without_monitoring' }
      it { client.should_not be_valid }
    end
  end

  describe '.filter' do
    let(:user) { create :user }
    let(:spfc){ build(:brand, name: 'spfc') }
    let(:hublot){ build(:brand, name: 'hublot') }
    let!(:data1) do
      create(:company,
             company_name: 'são paulo',
             brands: [spfc],
             annuity: 'may',
             status: 'active',
             document_type: :company,
             cnpj: '44.611.678/7362-71',
             user: user)
    end

    let!(:data2) do
      create(:company,
             company_name: 'acaliman',
             brands: [hublot],
             annuity: 'november',
             status: 'negotiating',
             document_type: :people,
             cpf: '352.777.458-07')
    end

    let!(:data3) do
      create(:company,
             company_name: 'nudesign',
             brands: [spfc, hublot],
             annuity: 'november',
             status: 'refused',
             status_refused: 'expensive_price')
    end

    it { Client.filter.should have(3).items }

    context 'filter_by_search' do
      context 'CPF/CNPJ' do
        it { Client.filter(search: '44.611.678/7362-71').should == [data1] }
        it { Client.filter(search: '352.777.458-07').should == [data2] }
      end

      context 'company name' do
        it { Client.filter(search: 'são paulo').should == [data1] }
        it { Client.filter(search: 'São paulo').should == [data1] }
        it { Client.filter(search: 'São Paulo').should == [data1] }
        it { Client.filter(search: 'Sao Paulo').should == [data1] }
        it { Client.filter(search: 'ácáliman').should == [data2] }
        it { Client.filter(search: 'ácáliman').should == [data2] }
      end

      context 'brand' do
        it { Client.filter(search: 'wrong brand').should be_empty }
        it { Client.filter(search: 'spfc').should == [data1, data3] }
        it { Client.filter(search: 'hublot').should == [data2, data3] }
      end
    end

    context 'filter_by_annuity' do
      context 'May' do
        it { Client.filter(annuity: 'may').should have(1).item }
        it { Client.filter(annuity: 'may').should == [data1] }
      end

      context 'November' do
        it { Client.filter(annuity: 'november').should have(2).item }
        it { Client.filter(annuity: 'november').should == [data2, data3] }
      end
    end

    context 'filter_by_status' do
      it { Client.filter(status: 'active').should have(1).item }
      it { Client.filter(status: 'active').should == [data1] }
      it { Client.filter(status: 'expensive_price').should have(1).item }
      it { Client.filter(status: 'expensive_price').should == [data3] }
    end

    context 'filter_by_document_type' do
      it { Client.filter(document_type: 'people').should have(1).item }
      it { Client.filter(document_type: 'people' ).should == [data2] }
    end

    context 'filter_by_user' do
      it { Client.filter(user_id: user.id.to_s).should have(1).item }
      it { Client.filter(user_id: user.id.to_s).should == [data1] }
    end

    # context 'filter_by_atendiment_status' do
    #   before do
    #     data1.sac.save
    #     data1.sac.update_attributes(status: SacStatus::CALL_BACK)
    #   end
    #   it { Client.filter(atendiment_status: '3').should have(1).item }
    #   it { Client.filter(atendiment_status: '3').should == [data1] }
    # end

    # context 'filter_by_atendiment_date' do
    #   before do
    #     data1.sac.save
    #     data1.sac.historics.create(FactoryGirl.attributes_for(:historic, created_at: '2000-06-01', description: 'bla', status: SacStatus::CALL_BACK))

    #     data2.sac.save
    #     data2.sac.historics.create(FactoryGirl.attributes_for(:historic, created_at: '2000-07-17', description: 'bla', status: SacStatus::CALL_BACK))
    #   end
    #   it { Client.filter(atendiment_date: ['01/01/2012', '01/11/2012']).should have(0).item }
    #   it { Client.filter(atendiment_date: ['01/06/2000', '16/07/2000']).should have(1).item }
    #   it { Client.filter(atendiment_date: ['01/06/2000', '30/07/2000']).should have(2).item }
    # end
  end
end
