require 'spec_helper'

describe NotificationProcessManager do
  let(:client) { create(:client) }

  describe '#process' do
    context 'without status' do
      let(:manager) { described_class.new(client) }

      it 'must not call clean and create methods' do
        described_class
          .any_instance
	  .should_not_receive(:clean)

        described_class
          .any_instance
	  .should_not_receive(:create)

        manager.process
      end

      it { expect{manager.process}.to_not change{Notification.count}  }
    end

    context 'with status' do
      let(:manager) { described_class.new(client, status) }
      let(:status) do
        [
	  Notification::Status::DELAYED,
	  Notification::Status::INTIME
	].sample
    end

      it { expect{manager.process}.to change{Notification.count}.by(1) }

      context 'notification fields' do
        before { manager.process }
	subject { Notification.last }
	
	its(:client) { is_expected.to eq(client) }
	its(:status) { is_expected.to eq(status) }
	its(:call_back_at) { is_expected.to_not be_nil }
	its(:notification_type) { is_expected.to eq(Notification::Type::PROCESS_VALIDITY) }
      end
    end
  end

  describe '#clean' do
    let(:manager) { described_class.new(client) }

    context 'without notifications' do
      it 'must not do anything' do
        expect{manager.clean}.to_not change{Notification.count}
      end
    end

    context 'with an old notification' do
      before do
        create(:notification,
		client: client,
	      	notification_type: notification_type)
      end

      context 'with PROCESS_VALIDITY type' do
        let(:notification_type) { Notification::Type::PROCESS_VALIDITY }

        it 'must clean notifications' do
          expect{manager.clean}.to change{Notification.count}.from(1).to(0)
        end
      end

      context 'with SCHEDULING type' do
        let(:notification_type) { Notification::Type::SCHEDULING }

        it 'must not do anything' do
          expect{manager.clean}.to_not change{Notification.count}
        end
      end
    end
  end
end
