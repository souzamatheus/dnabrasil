require 'spec_helper'

describe ClientFileManager do
  describe ".prepare_attributes" do
    subject {described_class.prepare_attributes(attributes)}

    context 'without a client_files_attributes key' do
      let(:attributes) {{some: "values"}}
      it {should == attributes}
    end

    context 'with a client_files_attributes key' do
      context 'without file key' do
        let(:attributes) {{
          some: "values",
          client_files_attributes: {
            another: {key: "value"}
          }
        }}

        it {should == attributes}
      end

      context 'with file key' do
        let(:file) {double('File', original_filename: 'somefile.png')}
        let(:another_file) {double('File', original_filename: 'another.txt')}
        let(:attributes) {{
          some: "values",
          client_files_attributes: {
            "5" => {
              file: [
                file,
                another_file
              ]
            }
          }
        }}

        it {should == {
          some: "values",
          client_files_attributes: {
            "5" => {
              file: file,
              filename: 'somefile.png'
            },
            "6" => {
              file: another_file,
              filename: 'another.txt'
            }
          }
        }}
      end
    end
  end
end
