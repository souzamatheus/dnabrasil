# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contract do
    payment { Faker::Lorem.paragraphs }
  end
end
