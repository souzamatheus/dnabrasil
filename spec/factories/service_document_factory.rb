# encoding: UTF-8
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service_document do
    agency              "INPI"
    description         { Faker::Lorem.paragraphs }
    amount              100.0
    payment             "2x R$ 50,00"
    person_name         { Faker::Name.name }
    nationality         "Brasileira"
    relationship_status ServiceDocument::RelationshipStatus::MARRIED
    rg                  "11.111.111-11"
    cpf                 "735.389.809-77"
    birth_date          Date.today
    occupation          "Programador"
    position            "Senior"
    contact             "Rocky Balboa"
    agent               { Faker::Name.name }

    trait :client_informations do
      company_name    "Apple"
      client_document "01.000.000-0001/09"
      phone           "555-5555"
      address         "Rua tal"
      address_number  134
      neighborhood    "Vila Mariana"
      city            "São Paulo"
      state           "SP"
      postal_code     "03030001"
    end

    trait :complement do
      complement "apto 103 bloco 01"
    end
  end
end

