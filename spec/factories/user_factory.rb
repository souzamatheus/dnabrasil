# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    login                 {Faker::Internet.user_name}
    name                  'Test User'
    password              'please'
    password_confirmation 'please'
    role                  'operator'
  end

  trait :admin do
    role 'admin'
  end

  trait :manager do
    role 'manager'
  end

  trait :consultant do
    role 'consultant'
  end

  trait :operator do
    role 'operator'
  end
end
