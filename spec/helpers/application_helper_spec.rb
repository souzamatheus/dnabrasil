# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ApplicationHelper do

  describe "#formatted_cpf" do
    context 'with a valid number' do
      it { helper.formatted_cpf("80228352690").should == "802.283.526-90" }
    end

    context 'with a text instead of a number' do
      it { helper.formatted_cpf("wrong_cpf").should be_blank }
    end

    context 'with a invalid number' do
      it { helper.formatted_cpf("123456789").should be_nil }
    end
  end

  describe "#formatted_cnpj" do
    context 'with a valid number' do
      it { helper.formatted_cnpj("46039371594975").should == "46.039.371/5949-75" }
    end

    context 'with a text instead of a number' do
      it { helper.formatted_cnpj("wrong_cnpj").should be_blank }
    end

    context 'with a invalid number' do
      it { helper.formatted_cnpj("123456789").should be_nil }
    end
  end

  describe "#process_subtotal" do
    let(:client_process){ build_list :client_process, 4 }

    context 'with process' do
      let(:clients){ create_list :client, 2, client_processes: client_process }
      it { helper.process_subtotal(clients).should == 8 }
    end

    context 'without process' do
      let(:clients){ create_list :client, 2 }
      it { helper.process_subtotal(clients).should == 0 }
    end
  end

  describe 'client_status' do
    context 'when current_user is admin' do
      let(:client){FactoryGirl.create(:client)}

      before { helper.stub(:current_user).and_return(double(role: 'admin')) }

      it{ helper.client_status(client).should =~ /<p><select id="client_status_event" name="client\[status_event\]">/ }
      it{ helper.client_status(client).should =~ /option value="cancel">Cancelado/ }
      it{ helper.client_status(client).should =~ /option value="refuse">Recusado/ }
      it{ helper.client_status(client).should =~ /option value="negotiate">Negociação/ }
    end

    context 'when curremt_user is operator' do
      before { helper.stub(:current_user).and_return(double(role: 'operator')) }

      context 'when client status is negotiating' do
        let(:client){FactoryGirl.create(:client, status: 'negotiating')}

        it{ helper.client_status(client).should     =~ /option value="refuse">Recusado/ }
        it{ helper.client_status(client).should_not =~ /option value="cancel">Cancelado/ }
        it{ helper.client_status(client).should_not =~ /option value="negotiate">Negociação/ }
      end

      context "when client status is not negotiating" do
        let(:client){FactoryGirl.create(:client)}

        it{ helper.client_status(client).should_not =~ /<p><select id="client_status_event" name="client\[status_event\]">/ }
        it{ helper.client_status(client).should =~ /<p>Ativo<\/p>/ }
      end
    end
  end

  describe 'client_refused_status' do
    context 'when refused_status is unanswered' do 
      let(:client){FactoryGirl.create(:client)}

      before { helper.stub(:current_user).and_return(double(role: 'admin')) }

      it{ helper.client_status_refused(client).should =~ /<p><select id="client_status_refused_event" name="client\[status_refused_event\]">/ }
      it{ helper.client_status_refused(client).should =~ /option value="refused_by_service_price">Valor/ }
      it{ helper.client_status_refused(client).should =~ /option value="refused_by_checking_price">Somente Consulta/ }
      it{ helper.client_status_refused(client).should =~ /option value="refused_by_postpone">Postergado/ }
      it{ helper.client_status_refused(client).should =~ /option value="refused_by_without_autonomy">Sem Autonomia/ }
    end

    context 'when refused_status is answered' do
      let(:client){FactoryGirl.create(:client, status: 'refused', status_refused: 'expensive_price')}

      before { helper.stub(:current_user).and_return(double(role: 'admin')) }

      it{ helper.client_status_refused(client).should_not =~ /<p><select id="client_status_refused_event" name="client\[status_refused_event\]">/ }
      it{ helper.client_status_refused(client).should     =~ /<p>Valor<\/p>/ }
    end
  end

  describe '.m' do
    let(:client){ create :client, brand: '', status: 'negotiating'}
    it { helper.m(client.brand).should == '---' }
    it { helper.m(client.brand, '000').should == '000' }
    it { helper.m(client.company_name, '000').should == client.company_name }
  end
end
