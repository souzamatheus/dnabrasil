require 'spec_helper'

describe Generator::Contract::Clauses do

  it_should_behave_like "a clause without paragraphs",  1,  [1, 2, 3]
  it_should_behave_like "a clause without paragraphs",  3,  [2, 3]
  it_should_behave_like "a clause without paragraphs",  4,  [2, 3]
  it_should_behave_like "a clause without paragraphs",  5,  [1, 2, 3]
  it_should_behave_like "a clause without paragraphs",  6,  [1, 2, 3]
  it_should_behave_like "a clause without paragraphs",  7,  [3]
  it_should_behave_like "a clause without paragraphs",  8,  [3]
  it_should_behave_like "a clause without paragraphs",  9,  [3]
  it_should_behave_like "a clause without paragraphs",  10, [3]
  it_should_behave_like "a clause without paragraphs",  11, [1, 2, 3]
  it_should_behave_like "a clause without paragraphs",  12, [1, 2, 3]
  it_should_behave_like "a clause without paragraphs",  13, [1, 2, 3]

  it_should_behave_like "a clause with paragraphs",     2,  [1, 2, 3]
  it_should_behave_like "a clause with paragraphs",     3,  [1]
  it_should_behave_like "a clause with paragraphs",     4,  [1]
  it_should_behave_like "a clause with paragraphs",     7,  [1, 2]
  it_should_behave_like "a clause with paragraphs",     8,  [1, 2]
  it_should_behave_like "a clause with paragraphs",     9,  [1, 2]
  it_should_behave_like "a clause with paragraphs",     10, [1, 2]

  let(:service_document){
    create(:service_document, :client_informations,
            client:   create(:client),
            contract: build(:contract))
  }

  let(:document){Prawn::Document.new}
  let(:clauses){described_class.new(document, service_document)}
  let(:inspector){PDF::Inspector::Text.analyze(clauses.document.render)}

  describe "#add_clauses_to_document" do
    before {clauses.add_clauses_to_document}

    context "the title of the clauses" do
      it "some clauses should have a title" do
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13].each do |number| # skip 12
          parser = Prawn::Text::Formatted::Parser.to_array(clauses.get_text(number, "title"))
          inspector.strings.should include(parser[0][:text])
        end
      end

      it "the clauses 12 should not have a title" do
        clauses.get_text(12, "title").should be_blank
      end
    end

    context "the subtitle of the clauses" do
      it "all 13 clauses should have a subtitle" do
        (1..13).each do |number|
          parser = Prawn::Text::Formatted::Parser.to_array(clauses.get_text(number, "subtitle"))
          inspector.strings.join(' ').should include(parser[0][:text])
        end
      end
    end
  end
end
