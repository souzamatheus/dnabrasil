# encoding: UTF-8
require 'spec_helper'

describe ConflictFile::Base do
  let(:inpi_file){File.join(Rails.root, 'spec/support', 'inpi.xml')}
  let(:conflict_file) {described_class.new(inpi_file)}

  describe "#initialize" do
    it {conflict_file.should be_an_instance_of ConflictFile::Base }
  end

  describe "#extract_brands" do
    let(:informations) {conflict_file.extract_brands}

    it {informations.should have(4).itens}

    context 'LU&BE' do
      subject{informations[0]}

      it {subject.number.should == "840399995"}
      it {subject.date.should == "23/01/2013"}
      it {subject.class_name.should == "NCL(10) 33"}
      it {subject.company_name.should == "DOMINIO DE CAIR, S.L."}
      it {subject.inpi.should == "IPAS009"}
      it {subject.presentation.should == "Mista"}
      it {subject.nature.should == "De Produto"}
      it {subject.brand.should == "LU&BE"}
    end

    context 'JOHN LEWIS' do
      subject{informations[1]}

      it {subject.number.should == "840391110"}
      it {subject.date.should == "14/01/2013"}
      it {subject.class_name.should == "NCL(10) 11"}
      it {subject.company_name.should == "JOHN LEWIS PLC"}
      it {subject.inpi.should == "IPAS009"}
      it {subject.presentation.should == "Nominativa"}
      it {subject.nature.should == "De Produto"}
      it {subject.brand.should == "JOHN LEWIS"}
    end

    context 'DBS FALANTES&BE' do
      subject{informations[2]}

      it {subject.number.should == "905830202"}
      it {subject.date.should == "29/01/2013"}
      it {subject.class_name.should == "NCL(10) 09"}
      it {subject.company_name.should == "DARI BASSO SALGADO"}
      it {subject.inpi.should == "IPAS009"}
      it {subject.presentation.should == "Mista"}
      it {subject.nature.should == "De Produto"}
      it {subject.brand.should == "DBS FALANTES"}
    end

    context 'MADEBOY' do
      subject{informations[3]}

      it {subject.number.should == "840404352"}
      it {subject.date.should == "25/01/2013"}
      it {subject.class_name.should == "NCL(10) 35"}
      it {subject.company_name.should == "KATIA IOLANDA MADEIRA DE CARVALHO ME"}
      it {subject.inpi.should == "IPAS009"}
      it {subject.presentation.should == "Mista"}
      it {subject.nature.should == "De Serviço"}
      it {subject.brand.should == "MADEBOY"}
    end
  end
end
