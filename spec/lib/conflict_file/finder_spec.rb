# encoding: UTF-8
require 'spec_helper'

describe ConflictFile::Finder do
  context 'simple brand' do
    it_should_behave_like "a client with conflict", "Tricolor", "tricolor"
    it_should_behave_like "a client without conflict", "Tricolor", "Nothing"
  end

  context 'composed brand' do
    it_should_behave_like "a client with conflict", "Fernando & Mariano", "Fernando &"
    it_should_behave_like "a client with conflict", "Fernando & Mariano", "Fernando"
    it_should_behave_like "a client with conflict", "Fernando & Mariano", "& Mariano"
    it_should_behave_like "a client with conflict", "Fernando & Mariano", "Mariano"
    it_should_behave_like "a client with conflict", "Fernando & Mariano", "", "Fernando & Mariano"

    it_should_behave_like "a client without conflict", "Fernando & Mariano", "Pedro &"
    it_should_behave_like "a client without conflict", "Fernando & Mariano", "& Sorocaba"
    it_should_behave_like "a client without conflict", "Fernando & Mariano", "", "Pedro & Sorocaba"
  end
end
