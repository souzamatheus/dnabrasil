require 'spec_helper'

describe ConflictFile::Persist do
  describe ".find_and_save" do
    let(:info) {double(
      number:        "123",
      date:          "15/11/2012",
      class_name:    "559",
      company_name:  "Loldesign",
      inpi:          "0192837465",
      presentation:  "Adv",
      nature:        "De Produto",
      brand:         "SPFC.me",
      conflict:      "SPFC",
      client:        create(:client)
    )}

    subject { -> {described_class.new("some file").find_and_save} }

    before do
      ConflictFile::Finder.any_instance.stub(:matcher).and_return([info])
    end

    context 'without a file' do
      it {should change{Inpi::File.count}.by(1)}
      it {should change{Inpi::Entry.count}.by(1)}

      context 'file status' do
        before {subject.call}
        it {Inpi::File.last.should be_finished}
      end
    end

    context 'with an already created file' do
      before {described_class.new("some file").find_and_save}

      it 'should not create a new Inpi::File' do
        should_not change{Inpi::File.count}
      end

      it 'should not create any new entry on Inpi::Entry' do
        should_not change{Inpi::Entry.count}
      end

      context 'with a not finished file' do
        let(:file){Inpi::File.last}

        it 'should not create a new Inpi::File' do
          file.rollback!
          should change{file.reload.status}.to("finished")
        end
      end
    end
  end
end
