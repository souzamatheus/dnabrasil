# encoding: UTF-8
require 'spec_helper'

describe "prepare to create a new service authorization" do
  let!(:client){ create :client, user: user }

  before { disable_business_hours }

  context 'logged as operator ' do
    let!(:user){ create :user }

    it "doesn't see the button to create a new service authorization" do
      user_sign_in
      visit clients_path
      click_link client.company_name

      expect(page).to have_content("Somente Administradores podem acessar esta área.")
    end
  end

  context 'logged as manager' do
    let!(:user){ create :user, role: 'manager' }

    it "doesn't see the button to create a new service authorization" do
      user_sign_in
      visit clients_path
      click_link client.company_name

      expect(page).to have_content("Somente Administradores podem acessar esta área.")
    end
  end

  context 'logged as admin' do
    let!(:user){ create :user, role: 'admin' }

    it "see the button to create a new service authorization" do
      user_sign_in
      visit clients_path
      click_link client.company_name

      page.should have_link("Autorização")
      page.should have_link("Nova Autorização de Serviço")
    end
  end
end
