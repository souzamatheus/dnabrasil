# encoding: UTF-8
require 'spec_helper'

describe "logged in as ordinary user" do
  let!(:user){ create :user }
  let!(:client){ create :client, user: user }

  before { disable_business_hours }

  context "exit of the sac page without added a new entry on historic" do
    it "see the error message" do
      user_sign_in

      visit clients_path
      click_link client.company_name
      visit clients_path

      current_path.should == sac_path(client.id)
      page.should have_content "Você precisa deixar um histórico de atendimento nesse cliente!"
    end

    context "editing a user" do
      it "does not see the error message" do
        user_sign_in

        visit clients_path
        click_link client.company_name
        click_link "Editar"

        current_path.should == edit_client_path(client)
        page.should_not have_content "Você precisa deixar um histórico de atendimento nesse cliente!"
      end
    end
  end

  context "exit of the sac page after added a new entry on historic" do
    it "can leave the client page" do
      user_sign_in

      visit clients_path
      click_link client.company_name

      within('.form-painel.sac') do
        select 'Recado', from: 'sac_entry_status'
        fill_in 'sac_entry_description', with: 'Just a information about client'
      end

      within('.form-submit.sac') do
        click_button 'Salvar'
      end

      page.should have_content "Atendimento registrado com sucesso!"
      visit clients_path

      current_path.should == clients_path
      page.should_not have_content "Você precisa deixar um histórico de atendimento nesse cliente!"
    end
  end

  describe 'logged in as admin user' do
    before{ create :user, role: 'admin' }

    context 'exit of the sac page without added a new entry on historic' do
      it 'can leave the client page' do
        user_sign_in

        visit clients_path
        click_link client.company_name
        visit clients_path

        current_path.should == clients_path
        page.should_not have_content "Você precisa deixar um histórico de atendimento nesse cliente!"
      end
    end
  end
end
