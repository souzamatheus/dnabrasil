# encoding: utf-8
require 'spec_helper'

describe CustomServicesController do
  login_user
  disable_business_hours

  describe "GET cep" do
    context 'when valid cep' do
      let(:valid_response){ '{"bairro":"Cambuci","logradouro":"Rua Vicente de Carvalho","cep":"01521020","uf":"SP","localidade":"S\u00e3o Paulo"}' }

      before { get :cep, cep: '01521-020' }

      it { response.status.should == 200 }

      context 'json' do
        subject { JSON.parse(response.body) }

        its(["bairro"]) { should == 'Cambuci' }
        its(["logradouro"]) { should == 'Rua Vicente de Carvalho' }
        its(["cep"]) { should == '01521020' }
        its(["cidade"]) { should == 'São Paulo' }
        its(["estado"]) { should == 'SP' }
      end
    end

    context "when invalid cep" do
      before { get :cep, cep: '01521-0200000' }

      it { response.status.should == 500 }
    end
  end
end
