require 'spec_helper'

describe ReportsController do
  login_user('admin')

  describe "GET /relatorios" do
    it "displays the list of available clients" do
      client = double
      Client.should_receive(:paginate).and_return(client)

      get :index
      response.should render_template("index")
      assigns[:clients].should == client
    end
  end

  describe "GET /relatorios/filtro/:params" do
    let(:client) { mock_model(Client) }

    it "displays client" do
      Client.should_receive(:filter).and_return(client)
      client.should_receive(:paginate).and_return(client)

      get :filter, annuity: "may"
      response.should render_template("index")
      assigns[:clients].should == client
    end
  end
end
