require 'spec_helper'

describe GrowController do
  disable_business_hours

  describe 'as admin' do
    login_user('admin')

    describe "GET index" do
      let(:operator){ User.last }
      let(:entries){ double }
      let(:presenter){ double }

      it "get client json" do
        Notification
          .should_receive(:for)
          .with(operator)
          .and_return([entries])

        Notification
          .should_receive(:process_validity)
          .and_return([])

        NotificationPresenter
          .should_receive(:new)
          .with([entries])
          .and_return(presenter)

        presenter.should_receive(:json)

        get :index, format: :json
      end
    end
  end

  describe 'as operator' do
    login_user('operator')

    describe "GET index" do
      let(:operator){ User.last }
      let(:entries){ double }
      let(:presenter){ double }

      it "get client json" do
        Notification
          .should_receive(:for)
          .with(operator)
          .and_return([entries])

        Notification
          .should_not_receive(:process_validity)

        NotificationPresenter
          .should_receive(:new)
          .with([entries])
          .and_return(presenter)

        presenter.should_receive(:json)

        get :index, format: :json
      end
    end
  end

  describe "GET show" do
    login_user('operator')

    let(:entry){ double }
    let(:presenter){ double }

    it "get sac json" do
      Notification
        .should_receive(:find)
        .with("a1b2c3")
        .and_return(entry)

      NotificationPresenter
        .should_receive(:new)
        .with(entry)
        .and_return(presenter)

      presenter.should_receive(:sac_json)

      get :show, notification_id: 'a1b2c3', format: :json
    end
  end
end
