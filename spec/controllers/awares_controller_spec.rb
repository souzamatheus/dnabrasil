require 'spec_helper'

describe AwaresController do
  login_user('admin')

  describe "GET show" do
    let(:client){mock_model(Client)}
    let(:document){mock_model(ServiceDocument)}
    let(:contract){mock_model(Contract)}
    let(:generator){"pdf content"}
    let(:options){{
      type:         "application/pdf",
      filename:     "contrato.pdf",
      disposition:  "inline"
    }}

    before do
      Generator::AwareDocument::Base.any_instance.stub(:generate).and_return(generator)
      Client.should_receive(:find).with(client.to_param).and_return(client)
    end

    subject {get :show, client_id: client.to_param}

    it {subject.status.should == 200}
    it {subject.body.should == "pdf content"}
    it {subject.header["Content-Disposition"].should == "inline; filename=\"carta_de_ciencia.pdf\""}
    it {subject.header["Content-Transfer-Encoding"].should == "binary"}
    it {subject.header["Content-Type"].should == "application/pdf"}
  end
end
