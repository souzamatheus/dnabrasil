require 'spec_helper'

describe ClientsController do
  login_user 'operator'
  disable_business_hours

  describe "GET /clients" do
    it "displays the list of available clients" do
      client = double
      Client.should_receive(:paginate).and_return(client)
      get :index
      response.should render_template("index")
      assigns[:clients].should == client
    end

    context 'when operator is logged' do
      let(:operator){ User.where(role: 'operator').first }
      let!(:client) { create(:client, user: operator) }
      let!(:client2){ create(:client) }

      it 'display only operators client' do
        get :index
        assigns[:clients].should == [client]
        response.should render_template("index")
      end
    end
  end

  describe "GET /clientes/filtros" do
    it "displays the list of available clients" do
      client = double
      Client.stub_chain(:filter, :paginate).and_return(client)
      get :filter, search: 'param'
      response.should render_template("index")
      assigns[:clients].should == client
    end
  end

  describe "GET /clients/new" do
    it "displays the page to create a new clients" do
      client = mock_model(Client, client_processes: [], client_emails: [])
      Client.should_receive(:new).and_return(client)
      client.stub_chain(:client_processes, :build)
      client.stub_chain(:client_emails   , :build)

      get :new
      response.should render_template("new")
      assigns[:client].should == client
    end
  end

  describe "POST /clients" do
    context  let(:client) { mock_model(Client) }

    def do_post
      post :create, :client => {"these" => "params"}
    end

    context "with valid data" do
      before { client.stub(:save).and_return(true) }

      it "successfully creates a new client" do
        Client
          .should_receive(:new)
          .with("these" => "params").and_return(client)

        do_post
        response.should redirect_to(sac_path(client))
        flash[:notice].should == "Cliente criado com sucesso!"
      end
    end

    context "with invalid data" do
      before { client.stub(:save).and_return(false) }

      it "fails to create a new client" do
        Client
          .should_receive(:new)
          .with("these" => "params").and_return(client)

        do_post
        response.should render_template(:new)
        assigns[:client].should == client
      end
    end
  end

  describe "GET /clients/1/edit" do
    let(:client) do
      mock_model(Client, client_processes: [], client_emails: []).tap do |c|
        c.stub_chain(:client_images, :build)
      end
    end

    before do
      Client
        .should_receive(:find)
        .with(client.to_param).and_return(client)
      client.stub_chain(:client_processes, :build)
      client.stub_chain(:client_emails   , :build)
    end

    it "renders the form to update the client" do
      get :edit, :id => client.to_param

      response.should render_template("edit")
      assigns[:client].should == client
    end
  end

  describe "PUT /clients/1" do
    let(:user){ User.last }
    let(:client) { mock_model(Client) }

    before do
      request.env["HTTP_REFERER"] = edit_client_path(client)
      Client.stub(:find).with(client.to_param).and_return(client)
    end

    def do_put
      put :update, :id => client.to_param, :client => { "these" => "params" }
    end

    context "with valid data" do
      before do
        ClientFileManager.should_receive(:prepare_attributes)
          .with("these" => "params")
          .and_return("these" => "params")

        client
          .should_receive(:update_attributes)
          .with("these" => "params").and_return(true)
        user.constraints["blocked"].should be false
      end

      context "updates the client" do
        before {do_put}

        it {user.reload.constraints["blocked"].should be true}
        it {response.should redirect_to sac_path(client)}
        it {flash[:notice].should == "Cliente editado com sucesso!"}
      end

      context 'upload a file' do
        before do
          ActiveSupport::HashWithIndifferentAccess
            .any_instance
            .should_receive(:has_key?)
            .with(:client_images_attributes).and_return(false)

          ActiveSupport::HashWithIndifferentAccess
            .any_instance
            .should_receive(:has_key?)
            .with(:client_files_attributes).and_return(true)

          do_put
        end

        it {response.should redirect_to sac_path(client, anchor: :documents)}
        it {flash[:notice].should == "Novo documento adicionado com sucesso!"}
      end
    end

    context "with invalid data" do
      before do
        client
          .should_receive(:update_attributes)
          .with("these" => "params").and_return(false)
      end

      it "fails to update the client" do
        user.constraints["blocked"].should be false

        do_put
        user.reload.constraints["blocked"].should be false
        response.should render_template(:edit)
        assigns[:client].should == client
      end
    end
  end

  describe "DELETE destroy_file" do
    let(:file) {mock_model(ClientFile)}
    let(:client) {mock_model(Client, client_files: file)}

    before do
      Client.stub(:find).with(client.to_param).and_return(client)
      file.should_receive(:find).with("1234").and_return(file)
      file.should_receive(:destroy).and_return(true)

      delete :destroy_file, id: client.to_param, file_id: "1234"
    end

    it {response.should redirect_to sac_path(client, anchor: :documents)}
    it {flash[:notice].should == 'Arquivo Removido com Sucesso'}
  end

  describe "DELETE /client/1" do
    let!(:client) { create :client }

    it "deletes the client" do
      expect {
        delete(:destroy, id: client.to_param)
      }.to change { Client.count }.by(-1)
    end
  end
end
