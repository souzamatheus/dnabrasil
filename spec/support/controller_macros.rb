module ControllerMacros
  def login_user role=nil
    attributes = role ? { role: role } : {}

    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in User.last || create(:user, attributes)
    end
  end

  def disable_business_hours
    before(:each) do
      Settings.stub(:open?).and_return(true)
    end
  end
end
