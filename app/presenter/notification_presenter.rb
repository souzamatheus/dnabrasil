class NotificationPresenter
  include Rails.application.routes.url_helpers

  def initialize(entries)
    @entries = entries
  end

  def json
    @entries.inject([]) do |entries, entry|
      entries.push(to_hash(entry))
      entries
    end
  end

  def sac_json
    {
      sac_type: sac_type(@entries)
    }
  end

  private

  def to_hash(entry)
    client = entry.client

    {
      id: entry.to_param,
      notification_type: entry.notification_type_humanize,
      company_name: client.company_name,
      cpf: client.cpf,
      cnpj: client.cnpj,
      call_back_at: I18n.l(entry.call_back_at, format: :presenter),
      formatted_call_back_at: I18n.l(entry.call_back_at, format: :time),
      sac_type: sac_type(entry),
      link: sac_path(client),
      slug: client.slug
    }
  end

  def sac_type(entry)
    "#{Notification::Type.key_for(entry.notification_type)} #{entry.status || calculate_delay(entry)}"
  end

  def calculate_delay(entry)
    diff = -(Time.now - entry.call_back_at) / 60

    time_status = if diff < 0
      "delayed"
    elsif diff >= 0 && diff <= 5
      "intime"
    else
      "ahead"
    end
  end
end
