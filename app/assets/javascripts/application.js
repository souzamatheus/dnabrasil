// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.maskedinput-1.3.min
//= require jquery.timeago
//= require jquery.timeago.pt-br
//= require jquery.lol.search_cep
//= require mustache.min
//= require mootools/mootools-core
//= require mootools/mootools-more
//= require mootools/Picker
//= require mootools/Picker.Attach
//= require mootools/Picker.Date
//= require mootools/Locale
//= require jquery-ui
//= require_tree .

$(document).ready(function(){
  $.grow();

  var cepField = $('input.cep');

  if(cepField[0]){
    cepField.mask('99999-999');
    cepField.LOLSearchCep({
      logradouro:   '#client_address',
      bairro:       '#client_neighborhood',
      cidade:       '#client_city',
      uf:           '#client_state',
      cep:          'input.cep',
      focus_field:  '#client_number',
      url:          '/servicos/cep/'
    });
  }

  // $('#reports_annuity').on('change', filterAnnuity);

  new DNA.forms.AddMoreFieldsForm($('input[id*="client_client_processes_attributes"][type!="hidden"]'), "Processo");
  new DNA.forms.AddMoreFieldsForm($('input[id*="client_client_emails_attributes"][type!="hidden"]'), "Email");
  new DNA.forms.AddMoreFieldsForm($('input[id*="client_brands_attributes"][type!="hidden"]'), "Nova Marca");
  new DNA.forms.AddMoreFieldsForm($('input[id*="client_conflicts_attributes"][type!="hidden"]'), "Opções de Colidência");

  $('.mask-date').mask('99/99/9999');
  $('input[name*="cpf"]').mask('999.999.999-99');

  $('#client_status_event').on('change', statusClientCombo);

  observeClientTypeSelect();

  Locale.use("pt-BR");

  // # FIXME refatorar para aceitar vários campos. Exemplo logo abaixo
  // MOOTOOLS para campos de data/hora
  new Picker.Date($$('.mootools'), {
    minDate: new Date(),
    timePicker: true,
    positionOffset: {x: 5, y: 0},
    pickerClass: 'datepicker_dashboard',
    useFadeInOut: !Browser.ie,
    format: '%d/%m/%Y %H:%M',
    onSelect: function(date){
      $$('#sac_entry_call_back_at').set('value', date.format('%Y-%m-%d %H:%M'));
    }
  });

  // MOOTOOLS para campos de data
  $.each($('.mootools_date'), function(_, element){
    new Picker.Date($(element), {
      timePicker: false,
      positionOffset: {x: 5, y: 0},
      pickerClass: 'datepicker_dashboard',
      useFadeInOut: !Browser.ie,
      format: '%d/%m/%Y',
      onSelect: function(date){
        $(element)
          .closest('div.control-group')
          .next()
          .find('input[type="hidden"]')[0]
          .set('value', date.format('%Y-%m-%d'));
      }
    });
  });
});

function filterAnnuity(obj){
  var annuity = $(obj.target).val();
  var origin = window.location.origin;

  if (annuity){
    window.location.href = origin + "/relatorios/filtro/" + annuity
  }
}

function statusClientCombo(event) {
  var select            = $(event.target);
  var form              = select.parents('form').first();
  var refuse_container  = $('#refused-state-combo');
  var refuse_select     = refuse_container.find('select:first');

  if(select.val() === 'refuse') {
      refuse_container.show();
      refuse_select.on('change', statusRefusedClientCombo);
  }else if(select.val() !== 'refuse' && select.val() !== '') {
      hideAndResetSelect(refuse_container, refuse_select);
      form.submit();
  }else{
    hideAndResetSelect(refuse_container, refuse_select);
  }
}

function statusRefusedClientCombo(event) {
  var select    = $(event.target);
  var form      = select.parents('form').first();

  if(select.val() === ''){ return false;}
  form.submit();
}

function hideAndResetSelect(container, select){
  container.hide();
  select.val('');
  select.unbind('change');
}

function observeClientTypeSelect(){
  var select      = $('#client_document_type');
  var cnpj_field  = $('#client_cnpj');
  var cpf_field   = $('#client_cpf' );

  setClientType(select.val());

  cnpj_field.mask("99.999.999/9999-99");

  select.on('change', function(){
    setClientType(select.val());
  });
}

function setClientType(type){
  var cnpj_container = $('#client_cnpj').closest('.control-group'),
      cpf_container  = $('#client_cpf' ).closest('.control-group');

  if(type === 'company') {
    cnpj_container.show();
    cpf_container.hide();
  }else {
    cnpj_container.hide();
    cpf_container.show();
  }
}

