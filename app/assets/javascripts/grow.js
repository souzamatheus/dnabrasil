(function($, window){
  $.grow = function(){
    $.timeago.settings.allowFuture = true;

    _show = function(clients){
      $('body').append(_template(clients));
      $('.grow-info abbr').timeago();

      $.each($('.grow-item'), _animate);
      setInterval(_checkStatus, 20000);
    };

    _template = function(clients){
      return Mustache.to_html($("#grow-template").html(), _json(clients));
    };

    _json = function(clients) {
      return {
        clients: clients,

        document: function(){
          return this.cpf || this.cnpj;
        }
      };
    };

    _animate = function(i, element){
      var delay = 300;
      $(element).animate({left: 0}, delay * (i+1));
    };

    _checkStatus = function(){
      $.each($('.grow-item').not('.delayed'), _update);
    };

    _update = function(_, item){
      $.getJSON('/grow/' + $(item).data('id'), function(sac){
        $(item).addClass(sac.sac_type);
      });
    };

    _list = function() {
      $.getJSON('/grow', _show);
    }();
  };
})(jQuery, window);
