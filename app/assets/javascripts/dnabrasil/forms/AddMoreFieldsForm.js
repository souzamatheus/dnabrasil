window.DNA = {
  forms: {
    AddMoreFieldsForm: function(element, resource_name) {
      this.element        = element;
      this.resource_name  = resource_name;

      /*
       * Ao clicar no link "Adicionar X", baseado na estrutura do HTML,
       * o método vai clonar as informações do input anterior e criar um novo
       * wrapper idêntico ao acima, mudando somente o valor do atributo name,
       * para o Rails processar.
       *
       * Ex:(input anterior)
       * <input name="client[client_processes_attributes][0][number]">
       *
       * Retorno:
       * <input name="client[client_processes_attributes][1][number]">
       *
       * Por fim é dado um before() antes do link "Adicionar X"
       */
      var addField = function() {
        var input       = $(this).prev().find('input'),
            attributes  = inputAttributes(input),
            new_input   = createInput(input, {increment_number: true, field_name: attributes.field_name, size: '50'}),
            content     = $('<div>', { class: 'controls' }).css({position: 'relative'}),
            container   = $('<div>', { class: 'control-group string' }),
            label       = $('<label>', { class: "control-label", for: attributes.id })
                            .text(resource_name);

        content.append(new_input);
        content.append(removeFieldLink);

        container.append(label);
        container.append(content);

        $(this).before(container);
      }

      var removeField = function() {
        var parent        = $(this).closest('.control-group'),
            input         = $(this).prev(),
            input_hidden  = $("#" + input.attr("id").replace(/[a-z]+$/g, "id"))

        if (input_hidden.length == 0) {
          input.remove();
        }else {
          parent.append(createInput(input_hidden, {input_name: '_destroy', type: 'hidden', value: '1'}));
        }

        parent.fadeOut();
      }

      var inputAttributes = function(input, options) {
        options = $.extend({
          increment_number: false
        }, options);

        var input_name  = $(element).attr('name'),
            input_id    = $(element).attr('id'),
            indexOf     = input_id.indexOf("attributes"),
            id          = input_id.substring(0, indexOf),
            name        = input_name.split(']')[0] + "]",
            field_name  = input_name.match(/[a-z]+/g).pop(),
            number      = 0;

        if (input.is("input")){
          number = parseInt(input.attr('name').match(/\d/)[0]);
        }

        return {
          id:         id,
          name:       name,
          number:     options.increment_number ? number + 1 : number,
          field_name: field_name
        }
      }

      var createInput = function(input, options) {
        options = $.extend({
          field_name: "_destroy",
          type:       input.attr('type') || "text"
        }, options);

        var attributes  = inputAttributes(input, options),
            undescore   = options.field_name.indexOf("_"),
            input       = $('<input>', {
                            type: options.type,
                            id:   attributes.id + attributes.number + (undescore == -1 ? "_" + options.field_name : options.field_name),
                            name: attributes.name + "["+attributes.number+"]["+options.field_name+"]"
                          }).attr('value', options.value);

        return input;
      }

      removeFieldLink = function() {
        var link = $('<a>', {href: 'javascript://', class: 'less'}).text('-').on('click', removeField);
        return link;
      }

      addFieldLink = function() {
        var link = $('<a>', {href: 'javascript://', class: 'more'}).text('Adicionar '+resource_name).on('click', addField);
        return link;
      }

      this.init();
    }
  }
};

$.extend(DNA.forms.AddMoreFieldsForm.prototype, {
  appendableResource: function() {
    this.element.last().closest('.control-group').after(addFieldLink);
  },

  removableResource: function(_, element) {
    $(element).parent()
      .css({position: 'relative'})
      .append(removeFieldLink);
  },

  init: function() {
    $(this.element).each(this.removableResource);
    this.appendableResource();
  }
})
