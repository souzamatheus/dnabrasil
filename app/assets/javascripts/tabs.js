jQuery(document).ready(function($) {

  new tabManager();
  
});

var tabManager = function(){
  var that        = this;
  this.tabs_dom   = $('.tabs');

  this.startup = function(){
    this.tabs_dom.tabs();
    this.select_tab();
    this.dont_jump_page();
  },

  this.select_tab = function(){
    var startTabIndex = $("[data-tab-start]").attr('data-tab-start');

    if(startTabIndex !== undefined){
      this.tabs_dom.tabs( "select", parseInt(startTabIndex) );
    }
  },

  this.dont_jump_page = function(){
    setTimeout(function() {
      if (location.hash) {
        window.scrollTo(0, 0);
      }
    }, 1);
  }

  this.startup();
}