class CustomServicesController < ApplicationController
  def cep
    service_url = "http://api.postmon.com.br/v1/cep"
    clean_cep = params[:cep].gsub('-', '')
    json = JSON.parse(open("#{service_url}/#{clean_cep}").read)

    render json: json

  rescue Exception => e
    render json: {message: e.message}, status: :error
  end
end
