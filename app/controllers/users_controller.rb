#encoding: UTF-8
class UsersController < ApplicationController
  authorize_resource :except => [:configuration, :configuration_update]

  before_filter :load_user, except: [:index, :create]
  before_filter :all_users, only: [:index, :edit]

  def index
    @user = User.new
  end

  def create
    @user = User.new params[:user]

    if @user.save
      redirect_to users_path, :notice => "Usuário criado com sucesso!"
    else
      all_users
      render "index"
    end
  end

  def edit
  end

  def update
    if @user.update_attributes params[:user]
      redirect_to users_path, :notice => "Usuário editado com sucesso!"
    else
      render "edit"
    end
  end

  def configuration
    raise CanCan::AccessDenied if current_user != @user
  end

  def configuration_update
    raise CanCan::AccessDenied if current_user != @user

    if @user.update_attributes params[:user]
      redirect_to config_user_path(@user), :notice => "Usuário editado com sucesso!"
    else
      render "configuration"
    end
  end

  def reset
    if @user.reset_password
      redirect_to users_path, :notice => "Senha resetada com sucesso."
    else
      render "edit"
    end
  end

  def lock
    @user.lock_access!
    redirect_to users_path, notice: "Usuário #{@user.name} bloqueado com sucesso."
  end

  def unlock
    @user.unlock_access!
    redirect_to users_path, notice: "Usuário #{@user.name} desbloqueado com sucesso."
  end

  private

  def load_user
    @user = User.find(params[:id])
  end

  def all_users
    @users = User.all
  end
end
