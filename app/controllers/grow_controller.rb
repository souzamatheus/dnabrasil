class GrowController < ApplicationController
  def index
    @entries = Notification.for(current_user).to_a
    @entries += OutdatedClientProcessCacheManager.notifications if current_user.role == 'admin'

    respond_to do |format|
      format.json do
        render json: NotificationPresenter.new(@entries).json
      end
    end
  end

  def show
    @entry = Notification.find(params[:notification_id])

    respond_to do |format|
      format.json do
        render json: NotificationPresenter.new(@entry).sac_json
      end
    end
  end
end
