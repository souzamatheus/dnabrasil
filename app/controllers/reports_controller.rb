class ReportsController < ApplicationController
  authorize_resource :client

  def index
    @clients = Client.order_by(:company_name.asc).paginate(:page => params[:page])
  end

  def filter
    @clients = Client.filter(
                              annuity:            params[:annuity],
                              atendiment_status:  params[:atendiment_status],
                              document_type:      params[:document_type],
                              user_id:            params[:user],
                              status:             params[:status],
                              atendiment_date:   [params[:started_at], params[:finished_at]]
                            ).paginate(:page => params[:page], :per_page => params[:per_page].present? ? params[:per_page] : 10 )
    render 'index'
  end
end
