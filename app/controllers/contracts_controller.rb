class ContractsController < ApplicationController
  before_filter :load_client, :load_document

  def show
    send_data Generator::Contract::Base.new(@document).generate,
              type:        "application/pdf",
              filename:    "contrato.pdf",
              disposition: "inline"

  end

  def new
    @contract = @document.build_contract
  end

  def create
    @contract = @document.build_contract(params[:contract])

    if @contract.save
      TrackerDocument.register(@client, current_user, "Contract", "create")
      redirect_to service_authorizations_path(@client, anchor: :auth), :notice => "Contrato gerado com sucesso."
    else
      render "new"
    end
  end

  private

  def load_client
    @client = Client.find(params[:client_id])
    @sac = Sac.new
    @sac.historics.build
  end

  def load_document
    @document = @client.service_documents.find(params[:document_id])
  end
end
