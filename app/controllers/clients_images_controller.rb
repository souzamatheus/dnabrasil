class ClientsImagesController < ApplicationController
  before_filter :load_client

  def destroy
    image = @client.client_images.find(params[:id])
    image.destroy

    redirect_to sac_path(@client), :notice => "Logo removido com sucesso."
  end

  private

  def load_client
    @client = Client.find(params[:client_id])
  end
end
