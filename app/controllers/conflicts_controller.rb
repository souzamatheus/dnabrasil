#encoding: UTF-8
class ConflictsController < ApplicationController
  before_filter :load_file, only: [:show, :entry]

  def index
    @files = Inpi::File.all.order_by([:created_at, :desc])
  end

  def show
    @conflicts = @file.entries
  end

  def entry
    @entry = @file.entries.where(number: params[:id]).first
  end

  def create
    manager = InpiWorkerManager.new(params[:inpi_file_url])

    if manager.valid?
      InpiWorker.perform_async(params[:inpi_file_url])
      redirect_to conflicts_path, notice: "Arquivo adicionado com sucesso. Em instantes ele aparecerá na fila de processamento."
    else
      redirect_to conflicts_path, notice: "Arquivo inválido ou já existe."
    end
  end

  private

  def load_file
    @file = Inpi::File.find(params[:file_id])
  end
end
