# encoding: utf-8
class InpiMailer < ActionMailer::Base
  default from: "colidencia@dnabrasilmarcas.com.br", subject: "[DNA Brasil] - Arquivo de colidência finalizado"

  def process_done(filename)
    @filename = filename
    mail(to: "rafael@dnabrasilmarcas.com.br")
  end
end
