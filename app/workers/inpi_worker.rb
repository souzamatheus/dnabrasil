class InpiWorker
  include Sidekiq::Worker

  def perform(inpi_file_url)
    zip = ConflictFile::Ziper.new(inpi_file_url)
    zip.process

    filename = File.basename(zip.extracted_file)
    ConflictFile::Persist.new(zip.extracted_file).find_and_save
    InpiMailer.process_done(filename).deliver
  end
end