#encoding: UTF-8
class SacStatus < EnumerateIt::Base
  associate_values(
    :call_not_answered      => [1,  'Não atende'],
    :message                => [2,  'Recado'],
    :call_back              => [3,  'Retornar ligação'],
    :number_does_not_exist  => [4,  'Número não existe'],
    :success                => [5,  'Contato com sucesso'],
    :contact_does_not_exist => [6,  'Contato não existe'],
    :sent_email             => [7,  'Email enviado'],
    :process_progress       => [8,  'Movimentação de processo'],
    :post_office            => [9,  'Correios'],
    :internal_process       => [10, 'Movimentação interna'],
    :peek                   => [11, 'Consulta'],
    :serasa                 => [12, 'Serasa']
  )
end
