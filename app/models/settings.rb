module Settings
  extend self

  def open?
    now = Time.zone.now

    return false if now.saturday? || now.sunday?
    now >= morning && now <= afternoon
  end

  private

  def morning
    localtime(8, 45)
  end

  def afternoon
    localtime(18, 30)
  end

  def localtime(hour, minute)
    now = Time.zone.now
    Time.zone.local(now.year, now.month, now.day, hour, minute, 0)
  end
end
