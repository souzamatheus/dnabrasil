class Conflict
  include Mongoid::Document

  field :value
  embedded_in :client
end
