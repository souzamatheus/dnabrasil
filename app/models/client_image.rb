class ClientImage
  include Mongoid::Document
  include Mongoid::Timestamps

  mount_uploader :file, ClientUploader
  belongs_to :client
end
