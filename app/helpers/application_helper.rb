module ApplicationHelper

  def formatted_cpf(number)
    cpf = CPF.new(number)

    if cpf.valid?
      cpf.formatted
    else
      nil
    end
  end

  def formatted_cnpj(number)
   cnpj = CNPJ.new(number)

   if cnpj.valid?
     cnpj.formatted
   else
     nil
   end
  end

  def spaced_text text
    raw text.gsub(/\n/, '<br/>')
  end

  def process_subtotal clients
    clients
    .map(&:client_processes)
    .map(&:active)
    .map(&:size)
    .reduce(:+)
  end

  def client_status(client)
    content_tag(:p, render_status_for_client(client))
  end

  def client_status_refused(client)
    content_tag(:p, render_status_refused_for_client(client))
  end

  def all_status_to_client_for_select
    (Client.state_machines[:status].states.collect{|x| [x.human_name, x.name] } +
     Client.state_machines[:status_refused].states.collect{|x| ["Recusado: #{x.human_name}", x.name] }).sort
  end

  def m(attribute, mask='---')
    if attribute.present?
      attribute
    else
      mask
    end
  end

  private
    # TODO refatorar
    def render_status_for_client(client)
      case current_user.role
      when 'admin'
        render_status_select(:status_event, client.status_transitions, client.human_status_name)
      when 'operator'
        render_status_select_or_text(client)
      when 'consultant'
        render_status_select_or_text(client)
      end    
    end

    def render_status_select(field, transitions, status_name)
      collection_select(:client, field, transitions, :event, :human_to_name, :include_blank => status_name)
    end

    def render_status_select_or_text(client)
      if client.status == 'negotiating'
          render_status_select(:status_event, client.status_transitions.reject{|x| x.event != :refuse}, client.human_status_name)
      else
          client.human_status_name
      end
    end

    def render_status_refused_for_client(client)
      if client.unanswered?
        render_status_select(:status_refused_event, client.status_refused_transitions, client.human_status_refused_name)
      else
        client.human_status_refused_name
      end
    end
end
