class NotificationScheduleManager
  def initialize(client, user_id, call_back_at=nil)
    @client = client
    @user = User.find(user_id)
    @call_back_at = call_back_at
  end

  def process
    remove_old_notification
    create_new_notification
  end

  private

  def create_new_notification
    return false if @call_back_at.nil?

    Notification.create(user: @user,
                        client: @client,
                        call_back_at: @call_back_at,
		       	notification_type: Notification::Type::SCHEDULING)
  end

  def remove_old_notification
    notification = Notification.where(
			user: @user,
	    		client: @client,
		       	notification_type: Notification::Type::SCHEDULING).first

    notification.destroy if notification
  end
end
