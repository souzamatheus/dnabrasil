module ClientFileManager
  extend self

  def prepare_attributes(client_attributes, attr_key=:client_files_attributes)
    attrs = client_attributes.deep_dup

    if attrs.has_key?(attr_key)
      attrs[attr_key].each_pair do |key, values|
        add_filename_to_attributes(attr_key, attrs, key) if values.has_key?(:file)
      end
    end

    attrs
  end

  private

  def add_filename_to_attributes(attr_key, attributes, key)
    new_attributes = {}
    attributes[attr_key][key][:file].each_with_index do |file, index|
      new_attributes.merge!({"#{key.to_i + index}" => {
        file: file,
        filename: file.original_filename
      }})
    end

    attributes[attr_key] = attributes[attr_key].merge(new_attributes)
    attributes
  end
end
