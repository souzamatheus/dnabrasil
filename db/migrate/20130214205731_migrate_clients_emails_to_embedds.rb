class MigrateClientsEmailsToEmbedds < Mongoid::Migration
  def self.up
    Client.all.each do |client|
      client.client_emails.create email: client.email if client.email.present?
    end
  end

  def self.down
    Client.all.each do |client|
      client.client_emails.destroy_all
    end
  end
end