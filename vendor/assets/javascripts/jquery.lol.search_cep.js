(function($)
{
  $.fn.LOLSearchCep = function(params)
  {
    params = $.extend( { 
                        logradouro:      'input.logradouro'     , 
                        bairro:          'input.bairro'         ,
                        cidade:          'input.cidade'         ,
                        uf:              'select.uf'            ,
                        cep:             'input.cep'            ,
                        focus_field:     'input.number'         ,
                        url:             '/clientes/cep/'
                       }, params);

    this.each(function() {
      new Plugin(this, params);
    });

    return this;
  };
})(jQuery);

function Plugin(element, options) {
    var that        = this;
    this.el         = element;
    this.$el        = $(element);
    this.$loader    = undefined;
    this.$container = this.$el.closest('form');
    this.options    = options;

    // Initialize the plugin instance
    this.init();
}

//
// Plugin prototype
//
Plugin.prototype = {

    init: function() {
      var self = this;

      this._addLoader();
      this._lockInputs();

      this.$el.on('keyup paste', function(event){
        if (event.which != 9) {
          var target_content = $(event.target).val().replace('_', '');
          if(target_content.length === 9){
            self._getCepFromServer();
          }
        }
      });
    },

    _keyUp: function(event){
    },

    destroy: function() {},

    _addLoader: function() {
      this.$loader = $( "<img></img>", {
                          "src":   "/assets/loader.gif",
                          "class": "none loader"
                      }) ;

      this.$el.after( this.$loader );
    },

    _getCepFromServer: function(){
      var self = this;

      $.ajax({
        url: this.options.url + this.$el.val(),
        type: 'GET',
        dataType: 'json',
        beforeSend: function( jqXHR, settings ){
          self.$loader.toggleClass('none');
        },
        complete: function(xhr, textStatus) {
          self.$loader.toggleClass('none');
        },
        success: function(data, textStatus, xhr) {
          self._fillInputs(data);
        },
        error: function(xhr, textStatus, errorThrown) {
          var error_message = $.parseJSON(xhr.responseText);

          self._cleanInputs();
        }
      });
    },

    _fillInputs: function(json){
      this.$container.find(this.options.logradouro).val(json.logradouro);
      this.$container.find(this.options.bairro).val(json.bairro);
      this.$container.find(this.options.cidade).val(json.cidade);
      this.$container.find(this.options.uf).val(json.estado);
      this.$container.find(this.options.focus_field).focus();
      this._unlockInputs();
    },

    _cleanInputs: function(){
      this.$container.find(this.options.logradouro).val('');
      this.$container.find(this.options.bairro).val('');
      this.$container.find(this.options.cidade).val('');
      this.$container.find(this.options.uf).val('');
    },

    _lockInputs: function(){
      this.$container.find(this.options.logradouro).attr('disabled', 'disabled');
      this.$container.find(this.options.bairro).attr('disabled', 'disabled');
      this.$container.find(this.options.cidade).attr('disabled', 'disabled');
      this.$container.find(this.options.uf).attr('disabled','disabled');
    },

    _unlockInputs: function(){
      this.$container.find(this.options.logradouro).removeAttr('disabled');
      this.$container.find(this.options.bairro).removeAttr('disabled', '');
      this.$container.find(this.options.cidade).removeAttr('disabled', '');
      this.$container.find(this.options.uf).removeAttr('disabled','');
    }
}
